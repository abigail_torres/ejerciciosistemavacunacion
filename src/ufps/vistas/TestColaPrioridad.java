/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.vistas;

import Modelo.Persona;
import java.time.LocalDate;
import ufps.util.colecciones_seed.ColaP;

/**
 *
 * @author madar
 */
public class TestColaPrioridad {

    public static void main(String[] args) {

        ColaP<Persona> personas = new ColaP();

        Persona uno = new Persona(1, "Jose", LocalDate.of(1980, 3, 4), "algo@ufps.edu.co");
        Persona dos = new Persona(5, "Abigail", LocalDate.of(1980, 3, 5), "algo@ufps.edu.co");
        Persona tres = new Persona(100, "Ángel", LocalDate.of(1980, 4, 5), "algo@ufps.edu.co");

        //Debemos introducir las personas ordenadas por edad
        // abigail-jose-ángel
        //Cómo crear la prioridad??
        personas.enColar(uno, uno.getPrioridad()); //Jose
        personas.enColar(tres, tres.getPrioridad());// Angel
        personas.enColar(dos, dos.getPrioridad()); // abigail

        while (!personas.esVacia()) {
            System.out.println(personas.deColar().toString());
        }
    }
}
